To install

1) include the printnode library first "composer require PrintNode/printnode-php:dev-master"

2) Open your Laravel 5 application’s composer.json file and look for the "autoload" section. In "psr-4" insert the location to your package.

"autoload": {
	"classmap": [
		"database"
	],
	"psr-4": {
		"Dettlaffinc\\PrintNodeDettlaff\\": "packages/dettlaffinc/printnodedettlaff/src"
	}
},

3) run composer dump-autoload

4) Open your application’s config/app.php file and add it to your providers and aliases. This is where they both get really handy.

'providers' => [
	...
	Dettlaffinc\PrintNodeDettlaff\PrintNodeDettlaffServiceProvider::class,
],
 
'aliases' => [
	...
	'PrintNodeDettlaff' => Dettlaffinc\PrintNodeDettlaff\PrintNodeDettlaffFacade::class,
],

Class use:

PrintNodeDettlaff::getComputers();

PrintNodeDettlaff::getPrinters()

/**
* post print jobs
* @param	$arg	associative array
* - pass the data to be print
* - require keys and values:
* array(
	'printer' => 'the printer return on getPrinters() by array key',
	'contentType' => 'pdf_base64',
	'content' => '(url/pdf directory)',
	'source' => ' dettlaffinc package laravel',
	'title' => ' dettlaffinc print title ',
* )
* @return	$this->getRequest()
* -also it can return the following:
* 	// Returns the HTTP status code.
	->getStatusCode();
	* // Returns the HTTP status message.
	->getStatusMessage();
	* // Returns an array of HTTP headers.
		->getHeaders();
	* // Return the response body.
	->getContent();
* */
PrintNodeDettlaff::postPrintJob($arg)

main class use is : src/PrintNodeDettlaff.php