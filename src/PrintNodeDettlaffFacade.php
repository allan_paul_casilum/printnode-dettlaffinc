<?php
namespace Dettlaffinc\PrintNodeDettlaff;
use Illuminate\Support\Facades\Facade;

class PrintNodeDettlaffFacade extends Facade
{
    protected static function getFacadeAccessor() { 
        return 'dettlaffinc-printnodedettlaff';
    }
}
