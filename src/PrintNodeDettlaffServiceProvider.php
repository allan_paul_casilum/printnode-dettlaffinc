<?php
namespace Dettlaffinc\PrintNodeDettlaff;

use Illuminate\Support\ServiceProvider;

class PrintNodeDettlaffServiceProvider extends ServiceProvider
{
	/**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
 
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('dettlaffinc-printnodedettlaff', function() {
            return new PrintNodeDettlaff;
        });
        $this->mergeConfigFrom(
			__DIR__ . '/config.php', 'dettlaffinc-printnode-config'
		);
    }

}
